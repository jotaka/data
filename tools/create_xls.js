const fs = require( 'fs' );
var parse = require( 'csv-parse/lib/sync' );
const XLSX = require( 'xlsx' );

var indicators = fs.readFileSync( "../metadata/indicators.csv", "utf8" );
rows = parse( indicators, { columns: true, trim: true, delimiter: "," } );

if ( !rows ) {
  return false
}

console.log( 'Files to be created:', rows.length );
let counter = 0;
rows.forEach(
  ( indicatorRow ) => {

    if ( !fs.existsSync( "../indicators/" + indicatorRow.id + ".csv" ) ) {
      console.log( `No such file ${indicatorRow.id}.csv ...` );
      return false;
    }

    var content = XLSX.readFile( "../indicators/" + indicatorRow.id + ".csv" );
    XLSX.writeFile( content, "../xls/" + indicatorRow.id + ".xlsx" );
    counter++;
  }
);

console.log( 'Total files created:', counter );
