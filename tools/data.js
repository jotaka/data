const path = require("path")
const csv = require("fast-csv")

var stringify = require('csv-stringify');

const fs = require('fs')
var parse = require('csv-parse')

const _ = require('lodash')


fs.readdir( "../indicators/", function( err, files ) {

  files.forEach( function( file, index ) {
    readCSV(file)
  })
})

function readCSV(file){

  fs.readFile("../indicators/" + file, function (err, data) {
    parse(data, {columns: true, trim: true, delimiter: ","}, function(err, rows) {

      if(!rows){ return false }

      var header = rows[0];

      var headerIDS = _.keys(header);

      var columns = _.keys(header);
      columns.shift();
      columns.shift();

      console.log("test", columns)


      rows.forEach((row, index, allRows) => {
        columns.forEach((key) => {
        if(row[key] == "CO" || row[key] == "00" ){
        // console.log("aa", allRows[index][key])
        allRows[index][key] = ""
      }
    })

      // console.log("test", columns.indexOf("COUNTRY"))

      if(columns.indexOf("COUNTRY") !== -1){

        var hasData = false;
        columns.forEach((key) => {
          if(row[key] !== ""){
          hasData = true;
        }
      })

        if(hasData == false){
          allRows[index]["COUNTRY"] = "CO";
        }

      }

    })

      // console.log("test", rows)

      rows.unshift(headerIDS);

      stringify(rows, function(err, output){

        console.log("test", file)
        fs.writeFile("../indicators/" + file, output, function (err) {
          if (err) return console.log(err);
          console.log('DONE WRITING');
        });

      });

      // csvStream.pipe(writableStream);
      // csvStream.write(rows);
      // csvStream.end();


    })
  })

}


//
// var stream = fs.createReadStream("../indicators/1_1_1.csv");
//
// var csvStream = csv()
//
//   .transform(function(row){
//    
//     // console.log("test", row)
//
//     // console.log("test", )
//
//     return row;
//   })
//
//   .on("data", function(data){
//
//
//     console.log("Yey");
//
//     var csvStream = csv.format({headers: true}),
//       writableStream = fs.createWriteStream("my.csv");
//
//     writableStream.on("finish", function(){
//       console.log("DONE!");
//     });
//
//     csvStream.pipe(writableStream);
//     csvStream.write(data);
//     csvStream.end();
//
//     // data.map(d => console.log("a\n", d))
//
//   })
//   .on("end", function(data){
//
//
//
//
//   });
//
// stream.pipe(csvStream);
//
// //or


