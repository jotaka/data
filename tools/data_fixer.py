import csv
from glob import glob
import tempfile
import shutil



datafiles = glob("../indicators/*.csv")
for datafile in datafiles:	
	with tempfile.NamedTemporaryFile(mode='w+', delete=False,encoding='utf-8') as temp:
		with open(datafile,encoding='utf-8') as infile:
			reader = csv.reader(infile, delimiter=';', quotechar='"')
			writer = csv.writer(temp, delimiter=',',lineterminator='\n')		
			i = 0
			for rows in reader:	
				if(i == 0):
					rows.append("COUNTRY")
				else:
					rows.append("CO")
				writer.writerow(rows)
				i = i +1
		temp.flush()
	shutil.move(temp.name, datafile)		



